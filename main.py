# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import webapp2
import os
from google.appengine.ext.webapp import template
 
 # try to deploy it
class MainPage(webapp2.RequestHandler):
    def get(self):
        if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine/'):
            requests_host = 'http://localhost:3000'

        else:
            # production
            requests_host = ""

        template_params = {
            "requests_host": requests_host
        }

        html = template.render("appwords-app/dist/index.html", template_params)
        self.response.out.write(html)

app = webapp2.WSGIApplication([
    ('/', MainPage),
], debug=True)
